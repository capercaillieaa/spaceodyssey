﻿using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemyObjects;

    void Start()
    {
        StartCoroutine(SpawnEnemy());
    }

    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            var timeDelay = GetTimeDelay();
            yield return new WaitForSeconds(timeDelay);
            var enemy = GetEnemy();
            var startYCoord = GetStartYCoord();
            var newEnemy = Instantiate(enemy, new Vector2(11, startYCoord), Quaternion.identity);
            Kill(newEnemy);
        }
    }

    private float GetTimeDelay()
        => Random.Range(1f, 2.5f);

    private GameObject GetEnemy()
    {
        var maxEnemyIndex = enemyObjects.Length - 1;
        var enemyIndex = Random.Range(0, maxEnemyIndex);
        return enemyObjects[enemyIndex];
    }

    private float GetStartYCoord()
        => Random.Range(-2.5f, 2.5f);

    private void Kill(GameObject enemy)
        => Destroy(enemy, 10);
}
