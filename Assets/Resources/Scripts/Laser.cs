﻿using UnityEngine;

public class Laser : MonoBehaviour
{
    public int speed;

    private AudioSource shotAudio;

    private void Start()
    {
        shotAudio = GetComponent<AudioSource>();
        shotAudio.Play();
    }

    void Update()
    {
        if (gameObject.transform.position.x > 10)
            Kill(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
            Kill(gameObject);
    }

    private void Kill(GameObject gameObject)
        => Destroy(gameObject);
}
