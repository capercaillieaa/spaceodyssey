﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerShip : MonoBehaviour
{
    public int lives;
    public float force;
    public GameObject laserShot;

    private Rigidbody2D playerShipRigidbody;
    private Animator damageAnimator;
    private AudioSource loseAudio;
    private Text scoreText;
    private Text highScoreText;
    private Text livesText;
    private int scoreCount;
    private int highScore;
    private int livesCount;

    void Start()
    {
        playerShipRigidbody = GetComponent<Rigidbody2D>();
        damageAnimator = GetComponent<Animator>();
        loseAudio = GetComponent<AudioSource>();
        scoreText = GameObject.Find("ScoreCountLabel").GetComponent<Text>();
        highScoreText = GameObject.Find("HighScore").GetComponent<Text>();
        livesText = GameObject.Find("LivesCountLabel").GetComponent<Text>();
        scoreCount = 0;
        highScore = GetHightScore();
        livesCount = lives;
        UpdateScoreText();
        UpdateHighScoreText();
        UpdateLivesText();
        StartCoroutine(IncScore());
    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            CauseDamage();
            UpdateLivesText();
            Kill(collision.gameObject);
            if (livesCount == 0)
                RestartGame();
        }
        else if (collision.gameObject.CompareTag("Warning"))
            RestartGame();
    }

    private IEnumerator IncScore()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            scoreCount += 10;
            UpdateScoreText();
        }
    }

    private int GetHightScore()
    {
        int hightScore = 0;
        if (PlayerPrefs.HasKey("HighScore"))
            hightScore = PlayerPrefs.GetInt("HighScore");
        return hightScore;
    }

    private void SetHightScore()
    {
        PlayerPrefs.SetInt("HighScore", scoreCount);
        PlayerPrefs.Save();
    }

    private void UpdateScoreText()
        => scoreText.text = scoreCount.ToString();

    private void UpdateHighScoreText()
        => highScoreText.text = highScore.ToString();

    private void UpdateLivesText()
        => livesText.text = livesCount.ToString();

    public void FlyUp()
        => playerShipRigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);

    public void Shoot()
    {
        StartCoroutine(ShotCoroutine());
    }

    IEnumerator ShotCoroutine()
    {
        var newShot = Instantiate(laserShot, gameObject.transform.position, laserShot.transform.rotation);
        var shotSpeed = GetShotSpeed();
        MoveLaser(newShot, shotSpeed);
        yield return null;
    }

    private int GetShotSpeed()
        => laserShot.GetComponent<Laser>().speed;

    private void MoveLaser(GameObject laserShot, int speed)
        => laserShot.GetComponent<Rigidbody2D>().AddForce(Vector2.right * speed, ForceMode2D.Impulse);

    private void CauseDamage()
    {
        livesCount--;
        loseAudio.Play();
        damageAnimator.SetTrigger("DamageTrigger");
    }

    private void RestartGame()
    {
        Kill(gameObject);
        if (scoreCount > highScore) 
            SetHightScore();
        SceneManager.LoadScene(1);
    }

    private void Kill(GameObject gameObject)
        => Destroy(gameObject);

    public void AddScore(int score)
    {
        scoreCount += score;
        UpdateScoreText();
    }
}
