﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManagement : MonoBehaviour
{
    public void Shoot()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerShip>().Shoot();
    }

    public void FlyUp()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerShip>().FlyUp();
    }

}
