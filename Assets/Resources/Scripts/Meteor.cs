﻿using UnityEngine;

public class Meteor : MonoBehaviour
{
    public int lives;
    public int points;
    public GameObject detonator;

    private int meteorSpeed;
    private Animator damageAnimator;
    private Bang bang;

    private void Start()
    {
        meteorSpeed = GetMeteorSpeed();
        damageAnimator = GetComponent<Animator>();
        bang = detonator.GetComponent<Bang>();
    }

    void Update()
    {
        MoveMeteor();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            bang.Detonate(gameObject);
        if (collision.gameObject.CompareTag("Shot"))
        {
            if (lives == 1)
            {
                bang.Detonate(gameObject);
                UpdatePlyerPoints();
            }
            else
                CauseDamage();
        }
    }

    private int GetMeteorSpeed()
        => Random.Range(2, 5);

    private void MoveMeteor()
       => gameObject.transform.Translate(-meteorSpeed * Time.deltaTime, 0, 0);

    private void UpdatePlyerPoints()
    {
        var player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerShip>().AddScore(points);
    }

    private void CauseDamage()
    {
        lives--;
        damageAnimator.SetTrigger("DamageTrigger");
    }
}
