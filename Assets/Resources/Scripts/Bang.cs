﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bang : MonoBehaviour
{
    public void Detonate(GameObject enemy)
    {
        var bangPosition = GetEnemyPosition(enemy);
        var newBang = Instantiate(gameObject, bangPosition, Quaternion.identity);
        var bangAnimator = GetBangAnimator(newBang);
        var bangAudio = GetBangAudio(newBang);
        bangAudio.Play();
        bangAnimator.Play(0, 0);
        Destroy(enemy);
        Destroy(newBang, 1);
    }

    private Vector3 GetEnemyPosition(GameObject enemy)
        => enemy.transform.position;

    private Animator GetBangAnimator(GameObject bang)
        => bang.GetComponent<Animator>();

    private AudioSource GetBangAudio(GameObject bang)
    => bang.GetComponent<AudioSource>();
}
